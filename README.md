## How to install

Preferably project should be installed in a python virtual environment,
so install python virtualenv, then

```bash
git clone git@gitlab.com:holysmoke8/Ageism.git
virtualenv Ageism
```

Install the dependencies
```bash
cd Ageism
source bin/activate
pip install requirements.txt
```

All done

## How to run 

In 3 separate terminal windows, run redis, celery task queue and the flask app

### Redis
run-redis.sh script will install redis when run for the first time
```bash
./run-redis.sh
```

### Celery
```bash
bin/celery worker -A app.celery --loglevel=info
```

### Flask app
```bash
source bin/bash
python app.py
```