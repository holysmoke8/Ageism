import os
from flask import Flask, Request, jsonify
from flask import request, url_for, render_template
from flask import abort
from celery import Celery
import hashlib
import time
import random
from PIL import Image

UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 3 * 1024 * 1024

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@celery.task(bind=True)
def test_task(self, img_path):
    """
        Test task, delays execution randomly with random results
    """
    total = random.randint(10, 15)
    for i in range(total):
        if random.random() < 0.1:
            result = []
            for x in xrange(int(random.random() * 4 + 1)):
                print img_path
                img = Image.open(img_path)
                width, height = img.size
                face_width = int(random.random() * 100 + 50)
                result.append({'age': int(random.random() * 100),
                               'gender': random.choice(['Male', 'Female']),
                               'width': face_width,
                               'height': face_width,
                               'x': int(random.random() * (width - face_width)),
                               'y': int(random.random() * (height - face_width))
                               })
            return {'result': result}
        time.sleep(1)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def uplad_file():
    if 'file' not in request.form:
        return 'No file submitted.', 400
    file = request.form['file']
    img_hash = hashlib.md5(file).hexdigest()
    img_path = os.path.join(app.config['UPLOAD_FOLDER'], img_hash + '.png')
    with open(img_path, "wb") as f:
        f.write(file.decode('base64'))

    task = test_task.apply_async(args=[img_path])
    return jsonify({}), 202, {'Location': url_for('taskstatus',
                                                  task_id=task.id)}


@app.route('/status/<task_id>')
def taskstatus(task_id):
    task = test_task.AsyncResult(task_id)
    if task.state == 'SUCCESS':
        if not task.info or'result' not in task.info:
            response = {
                'state': task.state,
                'status': 'No faces detected!'
            }
        else:
            response = {
                'state': task.state,
                'status': 'Finished!',
                'result': task.info['result']
            }
    else:
        # something went wrong, dummy response for now
        response = {
            'state': task.state,
            'status': "Image couldn't be processed"
        }
    return jsonify(response)


if __name__ == '__main__':
    app.run(debug=True)
